package ru.cyber_eagle_owl.packviewer;

import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.util.List;

import eu.chainfire.libsuperuser.Shell;

/**
 * Created by User on 16.01.2018.
 */

public class RootHelper {

    private static final String TAG = "RootHelper";

    @Nullable
    private static String executeCommand(String command) {
        List<String> stdout = Shell.SU.run(command);
        if (stdout == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String line : stdout) {
            stringBuilder.append(line).append("\n");
        }
        return stringBuilder.toString();
    }

    public static boolean uninstall(String packageName) {
        String output = executeCommand("pm uninstall " + packageName);
        if (output != null && output.toLowerCase().contains("success")) {
            Log.i(TAG, "public static boolean uninstall return true, because command output (" + output + ") contains word #success#");
            return true;
        } else {
            Log.i(TAG, "public static boolean uninstall return false, because command output (" + output + ") is not contains word #success#");
            return false;
        }
    }

    public static boolean uninstallSystem(File appApk) {
        executeCommand("mount -o rw,remount /system");
        executeCommand("rm " + appApk.getAbsolutePath());
        executeCommand("mount -o ro,remount /system");

        // проверяем, удалился ли файл
        String output = executeCommand("ls " + appApk.getAbsolutePath());

        if (output != null && output.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean install(String appPath) {
        String output = executeCommand("pm install " + appPath);
        if (output != null && output.toLowerCase().contains("success")) {
            Log.i(TAG, "public static boolean install return true, because command output (" + output + ") contains word #success#");
            return true;
        } else {
            Log.i(TAG, "public static boolean install return false, because command output (" + output + ") is not contains word #success#");
            return false;
        }
    }

    public static boolean isRootProvided() {
        return Shell.SU.available();
    }
}

/*
* ДОМАШНЕЕ ЗАДАНИЕ
* Реализуйте установку приложения с использованием Root, если он имеется на устройстве.
* Используйте для этого команду pm install путь_до_апк.
* */