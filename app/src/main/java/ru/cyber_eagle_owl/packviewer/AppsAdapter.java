package ru.cyber_eagle_owl.packviewer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 07.01.2018.
 */

public class AppsAdapter extends RecyclerView.Adapter<AppsAdapter.ViewHolder> {

    private List<AppInfo> apps = new ArrayList<>();
    private List<AppInfo> filteredApps = new ArrayList<>();

    private String query = "";

    public void setApps(List<AppInfo> apps) {
        this.apps = apps;
        filterApps();
    }

    private void filterApps() {
        filteredApps.clear();

        if (query.isEmpty()) {
            filteredApps.addAll(apps);
        } else {
            for (AppInfo app : apps) {
                if (app.getName().toLowerCase().contains(query)) {
                    filteredApps.add(app);
                }
            }
        }
    }

    public void setQuery(String query) {
        this.query = query;
        filterApps();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) { //создаём новую ячейку
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.view_item_app, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) { //привязываем данные к ячейке
        AppInfo appInfo = filteredApps.get(position);
        holder.iconIv.setImageDrawable(appInfo.getIcon());
        holder.nameTv.setText(appInfo.getName());
        holder.versionTv.setText(appInfo.getVersionName());
        holder.itemView.setTag(appInfo);
    }

    @Override
    public int getItemCount() { // возвращаем количество элементов списка
        return filteredApps.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder { // ViewHolder хранит информацию о ячейке
        private final ImageView iconIv;
        private final TextView nameTv;
        private final TextView versionTv;
        public ViewHolder(View itemView) {
            super(itemView);
            iconIv = itemView.findViewById(R.id.icon_iv);
            nameTv = itemView.findViewById(R.id.name_tv);
            versionTv = itemView.findViewById(R.id.version_tv);
        }
    }
}
