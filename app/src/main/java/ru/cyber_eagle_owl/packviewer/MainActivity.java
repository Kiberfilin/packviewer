package ru.cyber_eagle_owl.packviewer;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private AppsAdapter appsAdapter;
    private AppManager appManager;
    private SwipeRefreshLayout swipeRefreshLayout;

    private static final int REQUEST_CODE_PICK_APK = 1;
    private static final int OPERATION_INSTALL = 1;
    private static final int OPERATION_UNINSTALL = 2;

    private String apkPath;
    private AppInfo appInfo;

    private int operationCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.apps_rv);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);

        appManager = new AppManager(this);
        appsAdapter = new AppsAdapter();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(appsAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        recyclerView.addItemDecoration(itemTouchHelper);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        reloadApps();
    }

    private final SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            reloadApps();
        }
    };

    private void reloadApps() {
        appsAdapter.setApps(appManager.getInstalledApps());
        appsAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void showToast(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.search_item);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i(TAG, "Text: " + newText);
                appsAdapter.setQuery(newText.toLowerCase().trim());
                appsAdapter.notifyDataSetChanged();
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.install_item:
                startFilePickerActivity();
                //showToast("Нажали пункт меню: " + item.getTitle().toString());
                //Log.i(TAG, "Нажали " + item.getTitle().toString());
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_APK && resultCode == RESULT_OK) {
            String tmpApkPath = data.getStringExtra(FilePickerActivity.EXTRA_FILE_PATH);
            Log.i(TAG, "APK: " + tmpApkPath);
            startAppInstallation(tmpApkPath);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void startFilePickerActivity() {
        Intent intent = new Intent(this, FilePickerActivity.class);
        startActivityForResult(intent, REQUEST_CODE_PICK_APK);}

    private void startAppInstallation(String tmpApkPath) {
        operationCode = OPERATION_INSTALL;
        apkPath = tmpApkPath;
        RootCheckerAsyncTask rootCheckerAsyncTask = new RootCheckerAsyncTask(rootCheckerListener);
        rootCheckerAsyncTask.execute();
    }

    private void startAppUninstallation(AppInfo tmpAppInfo) {
        operationCode = OPERATION_UNINSTALL;
        appInfo = tmpAppInfo;
        RootCheckerAsyncTask rootCheckerAsyncTask = new RootCheckerAsyncTask(rootCheckerListener);
        rootCheckerAsyncTask.execute();
    }

    private final ItemTouchHelper.Callback itemTouchHelperCallback = new ItemTouchHelper.Callback() {

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return makeMovementFlags(ItemTouchHelper.ACTION_STATE_IDLE, ItemTouchHelper.END);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            AppInfo appInfo = (AppInfo) viewHolder.itemView.getTag();
            startAppUninstallation(appInfo);
        }
    };

    private void uninstallWithRoot(AppInfo appInfo) {
        UninstallAsyncTask uninstallAsyncTask = new UninstallAsyncTask(uninstallListener);
        uninstallAsyncTask.execute(appInfo);
    }

    private void installWithRoot(String apkPath) {
        InstallAsyncTask installAsyncTask = new InstallAsyncTask(installListener);
        installAsyncTask.execute(apkPath);
    }

    private void uninstallWithoutRoot(AppInfo appInfo) {
        Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
        intent.setData(Uri.parse("package:" + appInfo.getPackageName()));
        startActivity(intent);
        reloadApps();
    }

    private void installWithoutRoot(String apkPath) {

        Intent installIntent = new Intent(Intent.ACTION_VIEW);
        Uri uri;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider", new File(apkPath));
            installIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            uri = Uri.fromFile(new File(apkPath));
        }

        installIntent.setDataAndType(uri, "application/vnd.android.package-archive");
        installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // Создаст новый процесс

        startActivity(installIntent);
        reloadApps();

    }

    private final UninstallAsyncTask.UninstallListener uninstallListener = new UninstallAsyncTask.UninstallListener() {
        @Override
        public void onUninstalled() {
            appInfo = null;
            reloadApps();
        }

        @Override
        public void onFailed() {
            reloadApps();
        }
    };

    private final InstallAsyncTask.InstallListener installListener = new InstallAsyncTask.InstallListener() {
        @Override
        public void onInstalled() {
            apkPath = null;
            reloadApps();
        }

        @Override
        public void onFailed() {
            reloadApps();
        }
    };

    private final RootCheckerAsyncTask.RootCheckerListener rootCheckerListener = new RootCheckerAsyncTask.RootCheckerListener() {
        @Override
        public void onRootProvided() {
            showToast("Root provided!");
            switch (operationCode) {
                case OPERATION_INSTALL:
                    if (apkPath != null) {
                        installWithRoot(apkPath);
                    }
                    break;

                case OPERATION_UNINSTALL:
                    if (appInfo != null) {
                        uninstallWithRoot(appInfo);
                    }
                    break;
            }
        }

        @Override
        public void onRootNotProvided() {
            showToast("Root not provided!");
            switch (operationCode) {
                case OPERATION_INSTALL:
                    if (apkPath != null) {
                        installWithoutRoot(apkPath);
                        apkPath = null;
                    }
                    break;

                case OPERATION_UNINSTALL:
                    if (appInfo != null) {
                        uninstallWithoutRoot(appInfo);
                        appInfo = null;
                    }
                    break;
            }
        }
    };
}