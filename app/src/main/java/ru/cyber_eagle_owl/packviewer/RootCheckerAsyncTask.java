package ru.cyber_eagle_owl.packviewer;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/**
 * Created by User on 18.01.2018.
 */

public class RootCheckerAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final WeakReference<RootCheckerListener> rootCheckerWeakReference;

    public RootCheckerAsyncTask(RootCheckerListener rootCheckerListener) {
        this.rootCheckerWeakReference = new WeakReference<RootCheckerListener>(rootCheckerListener);
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        if (RootHelper.isRootProvided()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean isRootProvided) {
        super.onPostExecute(isRootProvided);

        RootCheckerListener rootCheckerListener = rootCheckerWeakReference.get();

        if (isRootProvided) {
            rootCheckerListener.onRootProvided();
        } else {
            rootCheckerListener.onRootNotProvided();
        }
    }

    public interface RootCheckerListener {
        void onRootProvided();

        void onRootNotProvided();
    }

}
