package ru.cyber_eagle_owl.packviewer;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/**
 * Created by User on 16.01.2018.
 */

public class InstallAsyncTask extends AsyncTask<String, Void, Boolean> {

    private final WeakReference<InstallListener> installWeakReference;

    public InstallAsyncTask(InstallListener installListener) {
        super();
        this.installWeakReference = new WeakReference<>(installListener);
    }

    @Override
    protected Boolean doInBackground(String... params) {
        String appPath = params[0];
            if (appPath != null) {
                //install
                return RootHelper.install(appPath);
            } else {
                return false;
            }
    }


    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        InstallListener installListener = installWeakReference.get();
        if (installListener != null) {
            if (result) {
                installListener.onInstalled();
            } else {
                    installListener.onFailed();
            }
        }
    }

    public interface InstallListener {
        void onInstalled();

        void onFailed();
    }
}
