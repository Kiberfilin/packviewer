package ru.cyber_eagle_owl.packviewer;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/**
 * Created by User on 16.01.2018.
 */

public class UninstallAsyncTask extends AsyncTask<AppInfo, Void, Boolean> {
    private final WeakReference<UninstallListener> uninstallListenerWeakReference;

    public UninstallAsyncTask(UninstallListener uninstallListener) {
        super();
        this.uninstallListenerWeakReference = new WeakReference<>(uninstallListener);
    }

    @Override
    protected Boolean doInBackground(AppInfo... params) {
        AppInfo appInfo = params[0];

        if (appInfo.isSystem()) {
            return RootHelper.uninstallSystem(appInfo.getApkFile());
        } else {
            return RootHelper.uninstall(appInfo.getPackageName());
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        // получаем "сильную" ссылку
        UninstallListener uninstallListener = uninstallListenerWeakReference.get();
        // проверяем на null
        if (uninstallListener != null) {
            // если uninstallListener не null, то вызываем соответствующий метод,
            // согласно тому, чему равен result (true либо false)
            if (result) {
                uninstallListener.onUninstalled();
            } else {
                uninstallListener.onFailed();
            }
        }
    }

    public interface UninstallListener {
        void onUninstalled();

        void onFailed();
    }
}
